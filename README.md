# Bootstrap
This repository is meant as a quick and easy way to setup a bash environment, vim as well as install a few essential programs.

## Programs
1. vim-gnome    - editor
2. ncdu         - disk usage analyser
3. htop         - process viewer
4. urxvt        - terminal emulator
5. tmux         - terminal multiplexer
6. ranger       - file manager


## Install 
```bash
1. 'git clone https://gitlab.com/Guanox/dotfiles.git ~/.dotfiles'
2. 'cd ~/.dotfiles'
3. './bootstrap'

## Inspiration
My entire tmux configuration and tm script is heavily inspired by https://github.com/nicknisi 