" Delete trailing white useful for Python and CoffeeScript ;)
function functions#DeleteTrailingWS()
    exe "normal mz"
    %s/\s\+$//ge
    exe "normal `z"
endfunc

" Helps with saving files with sudo perms
function functions#SudoSaveFile() abort
    execute (has('gui_running') ? '' : 'silent') 'write !env SUDO_EDITOR=tee sudo -e % >/dev/null'
    let &modified = v:shell_error
endfunction

" A wrapper function to restore the cursor position, window position,
" and last search after running a command.
function functions#Preserve(command)
    " Save the last search
    let last_search=@/
    " Save the current cursor position
    let save_cursor = getpos(".")
    " Save the window position
    normal H
    let save_window = getpos(".")
    call setpos('.', save_cursor)

    " Do the business:
    execute a:command

    " Restore the last_search
    let @/=last_search
    " Restore the window position
    call setpos('.', save_window)
    normal zt
    " Restore the cursor position
    call setpos('.', save_cursor)
endfunction

" Function to Watch for changes if buffer changed on disk
function functions#WatchForChanges(bufname, ...)
    " Figure out which options are in effect
    if a:bufname == '*'
        let id = 'WatchForChanges'.'AnyBuffer'
        " If you try to do checktime *, you'll get E93: More than one match for * is given
        let bufspec = ''
    else
        if bufnr(a:bufname) == -1
            echoerr "Buffer " . a:bufname . " doesn't exist"
            return
        end
        let id = 'WatchForChanges'.bufnr(a:bufname)
        let bufspec = a:bufname
    end
    if len(a:000) == 0
        let options = {}
    else
        if type(a:1) == type({})
            let options = a:1
        else
            echoerr "Argument must be a Dict"
        end
    end
    let autoread    = has_key(options, 'autoread')    ? options['autoread']    : 0
    let toggle      = has_key(options, 'toggle')      ? options['toggle']      : 0
    let disable     = has_key(options, 'disable')     ? options['disable']     : 0
    let more_events = has_key(options, 'more_events') ? options['more_events'] : 1
    let while_in_this_buffer_only = has_key(options, 'while_in_this_buffer_only') ? options['while_in_this_buffer_only'] : 0
    if while_in_this_buffer_only
        let event_bufspec = a:bufname
    else
        let event_bufspec = '*'
    end
    let reg_saved = @"
    "let autoread_saved = &autoread
    let msg = "\n"
    " Check to see if the autocommand already exists
    redir @"
    silent! exec 'au '.id
    redir END
    let l:defined = (@" !~ 'E216: No such group or event:')
    " If not yet defined...
    if !l:defined
        if l:autoread
            let msg = msg . 'Autoread enabled - '
            if a:bufname == '*'
                set autoread
            else
                setlocal autoread
            end
        end
        silent! exec 'augroup '.id
        if a:bufname != '*'
            "exec "au BufDelete    ".a:bufname . " :silent! au! ".id . " | silent! augroup! ".id
            "exec "au BufDelete    ".a:bufname . " :echomsg 'Removing autocommands for ".id."' | au! ".id . " | augroup! ".id
            exec "au BufDelete    ".a:bufname . " execute 'au! ".id."' | execute 'augroup! ".id."'"
        end
        exec "au BufEnter     ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHold   ".event_bufspec . " :checktime ".bufspec
        exec "au CursorHoldI  ".event_bufspec . " :checktime ".bufspec
        " The following events might slow things down so we provide a way to disable them...
        " vim docs warn:
        "   Careful: Don't do anything that the user does
        "   not expect or that is slow.
        if more_events
            exec "au CursorMoved  ".event_bufspec . " :checktime ".bufspec
            exec "au CursorMovedI ".event_bufspec . " :checktime ".bufspec
        end
    augroup END
    let msg = msg . 'Now watching ' . bufspec . ' for external updates...'
end
" If they want to disable it, or it is defined and they want to toggle it,
if l:disable || (l:toggle && l:defined)
    if l:autoread
        let msg = msg . 'Autoread disabled - '
        if a:bufname == '*'
            set noautoread
        else
            setlocal noautoread
        end
    end
    " Using an autogroup allows us to remove it easily with the following
    " command. If we do not use an autogroup, we cannot remove this
    " single :checktime command
    " augroup! checkforupdates
    silent! exec 'au! '.id
    silent! exec 'augroup! '.id
    let msg = msg . 'No longer watching ' . bufspec . ' for external updates.'
elseif l:defined
    let msg = msg . 'Already watching ' . bufspec . ' for external updates'
end
"echo msg
let @"=reg_saved
endfunction
