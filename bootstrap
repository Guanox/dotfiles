#!/usr/bin/env bash

####################################################
# CREATION     : 2016-08-18
# MODIFICATION : 2016-10-02
#
# Environment bootstrap script which:
# - Intalls programs
# - Links config files
#
####################################################

ARGS=($*)
SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

export SCRIPT_DIR

# print usage if no arguments supplied
if [[ ${#ARGS[@]} = 0 ]]; then
    c_install=1; c_link=1
fi

function usage () {
    echo "Usage :  $0 [options]

    Options:
    -h|help       Display this message
    -i|install    Install programs
    -l|link       Link configuration files
    -c|copy       During linking copy and symbolic links are available
    -b|bootstrap  Bootrap entire system"
}

function handle_install() {
    if [ "$(uname)" = "Linux" ]; then
        $SCRIPT_DIR/install
    fi
}

function handle_link() {
    if [ "$(uname)" = "Linux" ]; then
        $SCRIPT_DIR/link
    fi
}

#-----------------------------------------------------------------------
#  Handle command line arguments
#-----------------------------------------------------------------------

while getopts ":hlibc" opt
do
    case $opt in

        h|help     )  usage; exit 0   ;;

        i|install  )  c_install=1   ;;

        l|link     )  c_link=1   ;;

        c|copy     )  export o_copy=1   ;;

        b|bootstrap)  c_install=1; c_link=1 ;;

        * )  echo -e "\n  Option does not exist : $OPTARG\n" usage; exit 1   ;;

    esac
done
shift $((OPTIND-1))

if [[ $c_install ]]; then
    handle_install
fi

if [[ $c_link ]]; then
    handle_link
fi
