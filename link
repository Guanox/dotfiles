#!/usr/bin/env bash


####################################################
# CREATION     : 2016-08-18
# MODIFICATION : 2016-08-18
#
# Symbolic linking script which:
# - Determines config files
# - Links to $HOME and $HOME/.config
#
# Requirements:
# - find
#
####################################################

echo -e "\n\n#######################################"
echo -e "\n          Linking config files          "
echo -e "\n#######################################"

TARGETS=$(find -H "$SCRIPT_DIR" -maxdepth 4 -name '*.symlink')
BACKUP_DIR="$SCRIPT_DIR/backup"

function handle_file()
{
    local file="$1"
    local target="$2"

    read -rp "Do you wish to add $file?" yn
    case $yn in
        [Yy]* )

            # file exists and its not a link to my own config file
            if [ -e "$file" ] && [ ! "$(readlink -- "$file")" = "$target" ]; then
                mv "$file" "$BACKUP_DIR"
            fi

            if [[ $o_copy ]]; then
                select opt in link copy
                do
                    PS3="Choose a method: "
                    case $opt in
                        copy ) cp -f "$target" "$file"; break ;;
                        link ) ln -fs "$target" "$file"; break;;
                        *    ) echo "Please selection link or copy." ;;
                    esac
                done
            else
                ln -fs "$target" "$file"
            fi

            ;;
        [Nn]* ) ;;
        * ) ;;
    esac

    echo -e "\n"
}

if [ ! -d "$BACKUP_DIR" ]; then
    mkdir -p "$BACKUP_DIR"
fi

for target in $TARGETS ; do
    file="$HOME/.$( basename "$target" ".symlink" )"
    handle_file "$file" "$target"
done

# Delete backup dir if it is empty
if [ ! "$(ls -A "$BACKUP_DIR")" ]; then
    rm -rf "$BACKUP_DIR"
else
    echo "Overwritten config files can be found here -> $BACKUP_DIR"
fi
